reset

set title "SPICE SCORE"

set xlabel "Number of test"
set ylabel "Score"
set yrange [0.7:1.1]
set xtics ("6" 0, "7" 1, "23" 2, "24" 3, "25" 4) 
set ytics ("0.7" 0.7, "0.8" 0.8, "0.9" 0.9, "1"1) 
set grid

set boxwidth 0.9 relative
set style data histograms
set style fill solid 1.0 border -1


plot "../result/result_all_user_average_spice_only_espeak.txt" using 2 title "All user average on espeak"
replot "../result/result_all_user_average_spice_ibm_over_espeak_all_strategy.txt" using 2 title "All user average on ibm"
set term pngcairo
set output "../images/graph_all_user_average_spice_espeak_vs_ibm.png"
replot

# 6 1.0
# 7 1.0
# 23 0.942307710647583
# 24 0.9583011567592621
# 25 1.0

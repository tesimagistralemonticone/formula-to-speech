reset

set title "EDIT TREE DISTANCE"
set xlabel "Number of test"
set ylabel "Score"
set xrange [0:26]  
set yrange [0:5]
set xtics 1
set ytics 0.5 
set grid

# blue
set style line 1 \
    linecolor rgb '#0060ad' \
    linetype 1 linewidth 2 \
    pointtype 7 pointsize 1.5 \

# green    
set style line 2 \
    linecolor rgb '#29b929' \
    linetype 1 linewidth 2 \
    pointtype 7 pointsize 1.5

# black    
set style line 3 \
    linecolor rgb '#000' \
    linetype 1 linewidth 2 \
    pointtype 7 pointsize 1.5

# red
set style line 4 \
    linecolor rgb '#dc143c' \
    linetype 1 linewidth 2 \
    pointtype 7 pointsize 1.5


plot   "result_user_1_etd.txt" using ($1):($2)     with linespoints linestyle 1 title "user 1"
replot "result_user_2_etd.txt" using ($1):($2 + 1) with linespoints linestyle 2 title "user 2"
replot "result_user_3_etd.txt" using ($1):($2 + 2) with linespoints linestyle 3 title "user 3"
replot "result_user_4_etd.txt" using ($1):($2 + 3) with linespoints linestyle 4 title "user 4"

reset

set title "SPICE DISTANCE"

set xlabel "Number of test"
set ylabel "Score"
set xrange [0:26]
set yrange [0.7:1.1]
set xtics 2
set ytics 0.1
set grid

set boxwidth 0.9 relative
set style data histograms
set style fill solid 1.0 border -1


plot   "../result/result_all_user_average_spice_all_except_famous.txt" using 2 title "All user average - no famous"

set term pngcairo
set output "../images/graph_all_user_average_spice_histo.png"
replot
reset

set title "SPICE SCORE"

set xlabel "Number of test"
set ylabel "Score"
set xrange [0:25]
set yrange [0.7:1.1]
set xtics 2
set ytics 0.1
set grid

# blue
set style line 1 \
    linecolor rgb '#0060ad' \
    linetype 1 linewidth 2 \
    pointtype 7 pointsize 1.5 \

plot   "../result/result_all_user_average_spice_all_except_famous.txt" using ($1):($2) with linespoints linestyle 1 title "All user average except famous"

set term pngcairo
set output "../images/graph_all_user_average_spice_diff_col.png"
replot
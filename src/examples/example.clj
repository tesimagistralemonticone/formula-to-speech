(ns examples.example)


(def ex1 "<apply><neq/><ci>x</ci><cn type=\"integer\">0</cn></apply>")

(def ex2 "<apply><eq/><ci>y</ci><apply><plus/><ci>x</ci><cn type=\"integer\">1</cn></apply></apply>")

(def ex3 "<apply>\n  <eq/>\n  <ci>y</ci>\n  <apply>\n    <plus/>\n    <ci>x</ci>\n    <apply>\n      <minus/>\n      <cn>3</cn>\n      <cn type=\"integer\">1</cn>\n    </apply>\n  </apply>\n</apply>")

(def ex4 "<apply><eq/><ci>A</ci><apply><conditional-set/><ci>x</ci><apply><gt/><ci>x</ci><cn type=\"integer\">0</cn></apply></apply></apply>")

(def ex5 "<apply><eq/><ci>A</ci><apply><conditional-set/><apply><pair/><ci>x</ci><ci>y</ci></apply><apply><gt/><ci>X</ci><cn type=\"integer\">0</cn></apply></apply></apply>")

; x elevato a 2
(def ex6 "<apply><power/><ci>x</ci><cn type=\"integer\">2</cn></apply>")

(def ex7 "<apply><conditional-set/><apply><pair/><apply><minus/><ci>x</ci></apply><ci>y</ci></apply><apply><lt/><apply><abs/><apply><power/><ci>x</ci><cn type=\"integer\">2</cn></apply></apply><apply><power/><ci>y</ci><cn type=\"integer\">2</cn></apply></apply></apply>\n")
(def ex8 "<apply>\n<limit/>\n<bvar><ci> x </ci></bvar>\n<lowlimit><cn> a </cn></lowlimit>\n<apply><sin/><ci> x </ci></apply>\n</apply>\n")

(def ex9 "<apply><sum/><bvar><ci> x </ci></bvar><lowlimit><ci> a </ci></lowlimit><uplimit><ci> b </ci></uplimit><apply><sin/><ci> x </ci></apply></apply>")

(def ex10 "<apply><sum/><bvar><ci>x</ci></bvar><lowlimit><ci>a</ci></lowlimit><uplimit><ci>b</ci></uplimit><apply><ci>f</ci><ci>x</ci></apply></apply>")

(def ex11 "<apply><conditional-set/><apply><pair/><apply><minus/><ci>x</ci></apply><ci>y</ci></apply><apply><abs/><apply><lt/><apply><power/><ci>x</ci><cn type=\"integer\">2</cn></apply><apply><power/><ci>y</ci><cn type=\"integer\">2</cn></apply></apply></apply></apply>")

;; a - b / c * d
;(def sal0 "<apply><minus/><ci>a</ci><apply><times/><apply><divide/><ci>b</ci><ci>c</ci></apply><ci>d</ci></apply></apply>")
;; (a - b) / (c * d)
;(def sal1 "<apply><divide/><apply><minus/><ci>a</ci><ci>b</ci></apply><apply><times/><ci>c</ci><ci>d</ci></apply></apply>")
;; (a - b) / c * d
;(def sal2 "<apply><times/><apply><divide/><apply><minus/><ci>\uD835\uDC4E</ci><ci>\uD835\uDC4F</ci></apply><ci>\uD835\uDC50</ci></apply><ci>\uD835\uDC51</ci></apply>")
;

(def test1 "<apply><eq/><apply><limit/><bvar><ci> x </ci></bvar><lowlimit><apply><plus/><ci>infinito</ci></apply></lowlimit><apply><divide/><apply><ci>f</ci><ci>x</ci></apply><apply><ci>g</ci><ci>x</ci></apply></apply></apply><cn>0</cn></apply>")
(def test2 "<apply><eq/><ci>A</ci><apply><conditional-set/><apply><pair/><ci>x</ci><ci>y</ci></apply><apply><lt/><apply><power/><ci>x</ci><cn type=\"integer\">2</cn></apply><apply><power/><ci>y</ci><cn type=\"integer\">2</cn></apply></apply></apply></apply>")
(def test3 "<apply><sum/><bvar><ci>x</ci></bvar><lowlimit><ci>0</ci></lowlimit><uplimit><apply><plus/><ci>infinito</ci></apply></uplimit><apply><ci>f</ci><ci>x</ci></apply></apply>")
(def test4 "<apply><sum/><bvar><ci>x</ci></bvar><lowlimit><ci>0</ci></lowlimit><uplimit><apply><plus/><ci>infinito</ci></apply></uplimit><apply><subscript/><ci>x</ci><ci>i</ci></apply></apply>")

; A-B=A-(A\cap B)=A-(B\cap A)
(def set1 "<apply><eq/><apply><setdiff/><ci>A</ci><ci>B</ci></apply><apply><eq/><apply><setdiff/><ci>A</ci><apply><intersect/><ci>A</ci><ci>B</ci></apply></apply><apply><setdiff/><ci>A</ci><apply><intersect/><ci>B</ci><ci>A</ci></apply></apply></apply></apply>")

; x \in A \Longrightarrow x \geq 0
(def set2 "<apply><iff/><apply><notin/><ci>x</ci><ci>A</ci></apply><apply><geq/><ci>x</ci><cn>0</cn></apply></apply>")

;A\times B=\{(a,b)|a\in A,b\in B\}
(def cond-set1 "<apply><eq/><apply><cartesianproduct/><ci>A</ci><ci>B</ci></apply><apply><conditional-set/><apply><pair/><ci>x</ci><ci>y</ci></apply><apply><and/><apply><in/><ci>x</ci><ci>A</ci></apply><apply><in/><ci>y</ci><ci>B</ci></apply></apply></apply></apply>")

(def int1 "<apply><int/><bvar><ci>x</ci></bvar><lowlimit><cn>0</cn></lowlimit><uplimit><cn>1</cn></uplimit><apply><power/><ci>x</ci><cn>2</cn></apply></apply>")
(def int2 "<apply><int/><bvar><ci>x</ci></bvar><apply><power/><ci>x</ci><cn>2</cn></apply></apply>")

(def der1 "<apply><diff/><ci>f</ci></apply>")
(def der2 "<apply><diff/><bvar><ci>x</ci></bvar><apply><sin/><ci>x</ci></apply></apply>")
(def der3 "<apply><diff/><bvar><ci>x</ci><degree><cn>2</cn></degree></bvar><apply><power/><ci>x</ci><cn>4</cn></apply></apply>")

(def root1 "<apply><root/><ci>x</ci></apply>")
(def root2 "<apply><root/><degree><ci>n</ci></degree><ci>a</ci></apply>")


;(def sal "<apply><minus/><ci>a</ci><apply><times/> <apply><divide/><ci>b</ci><ci>c</ci></apply><ci>d</ci> </apply></apply>")

;(def sal "<apply><minus/><ci>a</ci><apply><times/><ci>b</ci><apply><times/><ci>c</ci><ci>d</ci></apply></apply></apply>")

;(def l "<apply><minus/><ci>a</ci><apply><times/><apply><times/><ci>b</ci><ci>c</ci></apply><ci>d</ci></apply></apply>")
;(def r "<apply><minus/><ci>a</ci><apply><times/><ci>b</ci><apply><times/><ci>c</ci><ci>d</ci></apply></apply></apply>")

; a - b / c * d     ===>  A meno ( ( b diviso c ) per d )
(def sal0 "<apply><minus/><ci>a</ci><apply><times/><apply><divide/><ci>b</ci><ci>c</ci></apply><ci>d</ci></apply></apply>")

; (a - b) / c * d   ===> ( a meno b diviso c ) per d
(def sal1 "<apply><times/><apply><divide/><apply><minus/><ci>a</ci><ci>b</ci></apply><ci>c</ci></apply><ci>d</ci></apply>")

;a - b / (c * d)    ===> A meno ( b diviso c per d )
(def sal2 "<apply><minus/><ci>a</ci><apply><divide/><ci>b</ci><apply><times/><ci>c</ci><ci>d</ci></apply></apply></apply>")

; (a - b) / (c * d) ===> A meno b diviso c per d
(def sal3 "<apply><divide/><apply><minus/><ci>a</ci><ci>b</ci></apply><apply><times/><ci>c</ci><ci>d</ci></apply></apply>")

;\sqrt {1 - \frac {a} {b}}
(def sal4 "<apply><root/><apply><minus/><cn>1</cn><apply><divide/><ci>a</ci><ci>b</ci></apply></apply></apply>")
;\sqrt {x}
(def sal5 "<apply><root/><apply><plus/><ci>x</ci><cn>1</cn></apply></apply>")
; \frac{a * (b + 1)}{c}
(def sal6 "<apply><divide/><apply><times/><ci>a</ci><apply><plus/><ci>b</ci><cn>1</cn></apply></apply><ci>c</ci></apply>")
;a * (b + 1)
(def sal7 "<apply><times/><ci>a</ci><apply><plus/><ci>b</ci><cn>1</cn></apply></apply>")
; (b + 1) * a
(def sal8 "<apply><times/><apply><plus/><ci>b</ci><cn>1</cn></apply><ci>a</ci></apply>")
;\sum_{i=0}^{+\infty}\sqrt{x_i}
(def sal9 "<apply><sum/><bvar><ci>x</ci></bvar><lowlimit><ci>0</ci></lowlimit><uplimit><apply><plus/><ci>infinito</ci></apply></uplimit><apply><root/><apply><subscript/><ci>x</ci><ci>i</ci></apply></apply></apply>")
;\sum_{i=0}^{+\infty}\sqrt{1 - \frac{x_i}{b} }
(def sal10 "<apply><sum/><bvar><ci>x</ci></bvar><lowlimit><ci>0</ci></lowlimit><uplimit><apply><plus/><ci>infinito</ci></apply></uplimit><apply><root/><apply><minus/><cn>1</cn><apply><divide/><apply><subscript/><ci>x</ci><ci>i</ci></apply><ci>b</ci></apply></apply></apply></apply>")
;\sum_{i=0}^{+\infty} x_i + y_i
(def sal11 "<apply><sum/><bvar><ci>x</ci></bvar><lowlimit><ci>0</ci></lowlimit><uplimit><apply><plus/><ci>infinito</ci></apply></uplimit><apply><plus/><apply><subscript/><ci>x</ci><ci>i</ci></apply><apply><subscript/><ci>y</ci><ci>i</ci></apply></apply></apply>")



; A\times B=\{(x,y) \mid x\in A,y\in B\}
(def easy-1-old "<apply><eq/><apply><cartesianproduct/><ci>A</ci><ci>B</ci></apply><apply><conditional-set/><apply><pair/><ci>x</ci><ci>y</ci></apply><apply><and/><apply><in/><ci>x</ci><ci>A</ci></apply><apply><in/><ci>y</ci><ci>B</ci></apply></apply></apply></apply>")

; \sum_{k=1}^{n}k=\frac{n(n+1)}{2}
(def easy-2-old "<apply><eq/><apply><sum/><bvar><ci>k</ci></bvar><lowlimit><cn>1</cn></lowlimit><uplimit><ci>n</ci></uplimit><ci>k</ci></apply><apply><divide/><apply><times/><ci>n</ci><apply><plus/><ci>n</ci><cn>1</cn></apply></apply><cn>2</cn></apply></apply>")

; \lim_{x\to+\infty}\frac{f(x)}{|g(x)|}=+\infty
(def easy-3-old "<apply><limit/><bvar><ci> x </ci></bvar><lowlimit><apply><plus/><ci>infinito</ci></apply></lowlimit><apply><eq/><apply><divide/><apply><ci>f</ci><ci>x</ci></apply><apply><abs/><apply><ci>g</ci><ci>x</ci></apply></apply></apply><apply><plus/><ci>infinito</ci></apply></apply></apply>")

;\lim_{x\to x_{0}}\frac{e^{x}-e^{x_{0}}}{\displaystyle x-x_{0}}=e^{x_{0}}
(def easy-4-old "<apply><limit/><bvar><ci>x</ci></bvar><lowlimit><apply><subscript/><ci>x</ci><cn>0</cn></apply></lowlimit><apply><eq/><apply><divide/><apply><minus/><apply><power/><ci>y</ci><ci>x</ci></apply><apply><power/><ci>y</ci><ci>x</ci></apply></apply><apply><minus/><ci>x</ci><apply><subscript/><ci>x</ci><cn>0</cn></apply></apply></apply><apply><power/><ci>y</ci><apply><subscript/><ci>x</ci><cn>0</cn></apply></apply></apply></apply>")

;f(x)=f(x_{0})+f^{\prime}(c)(x-x_{0})
(def easy-5-old "<apply><eq/><apply><ci>f</ci><ci>x</ci></apply><apply><plus/><apply><ci>f</ci><apply><subscript/><ci>x</ci><cn>0</cn></apply></apply><apply><times/><apply><diff/><apply><ci>f</ci><ci>c</ci></apply></apply><apply><minus/><ci>x</ci><apply><subscript/><ci>x</ci><cn>0</cn></apply></apply></apply></apply></apply>")


(def easy-6-old "<apply><eq/><apply><int/><bvar><ci>x</ci></bvar><lowlimit><ci>b</ci></lowlimit><uplimit><ci>c</ci></uplimit><ci>d</ci></apply><apply><times/><ci>d</ci><apply><minus/><ci>c</ci><ci>b</ci></apply></apply></apply>")


(def easy-7-old "<bind><forall/><bvar><ci>ε</ci></bvar><bind><exists/><bvar><ci>M</ci></bvar><apply><implies/><apply><gt/><ci>n</ci><ci>M</ci></apply><apply><gt/><apply><subscript/><ci>x</ci><ci>n</ci></apply><ci>ε</ci></apply></apply></bind></bind>")



; GOOOD

; A\times B=\{(x,y) \mid x\in A,y\in B\}
(def easy-ibm-smart-1    "<apply><eq/><apply><cartesianproduct/><ci>A</ci><ci>B</ci></apply><apply><conditional-set/><apply><pair/><ci>x</ci><ci>y</ci></apply><apply><and/><apply><in/><ci>x</ci><ci>A</ci></apply><apply><in/><ci>y</ci><ci>B</ci></apply></apply></apply></apply>")
(def easy-espeak-smart-1 "<apply><eq/><apply><cartesianproduct/><ci>C</ci><ci>D</ci></apply><apply><conditional-set/><apply><pair/><ci>z</ci><ci>y</ci></apply><apply><and/><apply><in/><ci>z</ci><ci>C</ci></apply><apply><in/><ci>y</ci><ci>D</ci></apply></apply></apply></apply>")

;; \sum_{k=1}^{n}k=\frac{n(n+1)}{2}
;(def easy-2 "<apply><eq/><apply><sum/><bvar><ci>k</ci></bvar><lowlimit><cn>1</cn></lowlimit><uplimit><ci>n</ci></uplimit><ci>k</ci></apply><apply><divide/><apply><times/><ci>n</ci><apply><plus/><ci>n</ci><cn>1</cn></apply></apply><cn>2</cn></apply></apply>")

(def easy-ibm-smart-2 "<apply><eq/><apply><inverse/><apply><ci>g</ci><ci>y</ci></apply></apply><apply><inverse/><apply><ci>f</ci><apply><divide/><apply><minus/><ci>y</ci><ci>b</ci></apply><ci>a</ci></apply></apply></apply></apply>")

; \int_{a}^{b}c\mbox{d}x=c(b-a)
(def easy-ibm-smart-3 "<apply><eq/><apply><int/><bvar><ci>x</ci></bvar><lowlimit><ci>b</ci></lowlimit><uplimit><ci>c</ci></uplimit><ci>d</ci></apply><apply><times/><ci>d</ci><apply><minus/><ci>c</ci><ci>b</ci></apply></apply></apply>")

(def easy-ibm-smart-4 "<apply><implies/><apply><gt/><ci>x</ci><ci>b</ci></apply><apply><lt/><apply><abs/><apply><ci>f</ci><ci>x</ci></apply></apply><ci>M</ci></apply></apply>")

(def easy-espeak-smart-4 "<apply><implies/><apply><gt/><ci>z</ci><ci>k</ci></apply><apply><lt/><apply><abs/><apply><ci>g</ci><ci>z</ci></apply></apply><ci>C</ci></apply></apply>")

(def easy-ibm-smart-5 "<apply><eq/><apply><root/><degree><ci>n</ci></degree><ci>x</ci></apply><apply><power/><ci>x</ci><apply><divide/><cn>1</cn><ci>n</ci></apply></apply></apply>")

;\item $$\lim_{x\to x_{0}}\left\{\frac{f(x)-f(x_{0})}{x-x_{0}}-f^{\prime}(x_{0})\right\}=0
(def hard-ibm-pause-1 "<apply><eq/><apply><limit/><bvar><ci>x</ci></bvar><lowlimit><apply><subscript/><ci>x</ci><cn>0</cn></apply></lowlimit><apply><minus/><apply><divide/><apply><minus/><apply><ci>f</ci><ci>x</ci></apply><apply><ci>f</ci><apply><subscript/><ci>x</ci><cn>0</cn></apply></apply></apply><apply><minus/><ci>x</ci><apply><subscript/><ci>x</ci><cn>0</cn></apply></apply></apply><apply><diff/><apply><ci>f</ci><apply><subscript/><ci>x</ci><cn>0</cn></apply></apply></apply></apply></apply><ci>0</ci></apply>")
(def hard-ibm-par-1 "<apply><eq/><apply><limit/><bvar><ci>z</ci></bvar><lowlimit><apply><subscript/><ci>z</ci><cn>0</cn></apply></lowlimit><apply><minus/><apply><divide/><apply><minus/><apply><ci>g</ci><ci>z</ci></apply><apply><ci>g</ci><apply><subscript/><ci>z</ci><cn>0</cn></apply></apply></apply><apply><minus/><ci>z</ci><apply><subscript/><ci>z</ci><cn>0</cn></apply></apply></apply><apply><diff/><apply><ci>g</ci><apply><subscript/><ci>z</ci><cn>0</cn></apply></apply></apply></apply></apply><ci>0</ci></apply>")
(def hard-ibm-smart-1 "<apply><eq/><apply><limit/><bvar><ci>y</ci></bvar><lowlimit><apply><subscript/><ci>y</ci><cn>0</cn></apply></lowlimit><apply><minus/><apply><divide/><apply><minus/><apply><ci>h</ci><ci>y</ci></apply><apply><ci>h</ci><apply><subscript/><ci>y</ci><cn>0</cn></apply></apply></apply><apply><minus/><ci>y</ci><apply><subscript/><ci>y</ci><cn>0</cn></apply></apply></apply><apply><diff/><apply><ci>h</ci><apply><subscript/><ci>y</ci><cn>0</cn></apply></apply></apply></apply></apply><ci>0</ci></apply>")
(def hard-espeak-pause-1 "<apply><eq/><apply><limit/><bvar><ci>x</ci></bvar><lowlimit><apply><subscript/><ci>x</ci><cn>0</cn></apply></lowlimit><apply><minus/><apply><divide/><apply><minus/><apply><ci>h</ci><ci>x</ci></apply><apply><ci>h</ci><apply><subscript/><ci>x</ci><cn>0</cn></apply></apply></apply><apply><minus/><ci>x</ci><apply><subscript/><ci>x</ci><cn>0</cn></apply></apply></apply><apply><diff/><apply><ci>h</ci><apply><subscript/><ci>x</ci><cn>0</cn></apply></apply></apply></apply></apply><ci>0</ci></apply>")

;y=f(a)+\frac{f(b)-f(a)}{b-a}(x-a)
(def hard-ibm-pause-2 "<apply><eq/><ci>y</ci><apply><plus/><apply><ci>f</ci><ci>c</ci></apply><apply><times/><apply><divide/><apply><minus/><apply><ci>f</ci><ci>d</ci></apply><apply><ci>f</ci><ci>c</ci></apply></apply><apply><minus/><ci>d</ci><ci>c</ci></apply></apply><apply><minus/><ci>x</ci><ci>c</ci></apply></apply></apply></apply>")
(def hard-ibm-par-2 "<apply><eq/><ci>y</ci><apply><plus/><apply><ci>g</ci><ci>b</ci></apply><apply><times/><apply><divide/><apply><minus/><apply><ci>g</ci><ci>c</ci></apply><apply><ci>g</ci><ci>b</ci></apply></apply><apply><minus/><ci>c</ci><ci>b</ci></apply></apply><apply><minus/><ci>z</ci><ci>b</ci></apply></apply></apply></apply>")
(def hard-ibm-smart-2 "<apply><eq/><ci>y</ci><apply><plus/><apply><ci>h</ci><ci>s</ci></apply><apply><times/><apply><divide/><apply><minus/><apply><ci>h</ci><ci>t</ci></apply><apply><ci>h</ci><ci>s</ci></apply></apply><apply><minus/><ci>t</ci><ci>s</ci></apply></apply><apply><minus/><ci>z</ci><ci>s</ci></apply></apply></apply></apply>")

(def hard-ibm-pause-3 "<apply><eq/><apply><int/><bvar><ci>x</ci></bvar><apply><divide/><cn type=\"integer\">1</cn><apply><root/><apply><minus/><apply><power/><ci>k</ci><cn type=\"integer\">2</cn></apply><apply><power/><ci>x</ci><cn type=\"integer\">2</cn></apply></apply></apply></apply></apply><apply><plus/><apply><arcsin/><apply><divide/><ci>x</ci><ci>k</ci></apply></apply><ci>c</ci></apply></apply>")
(def hard-ibm-par-3 "<apply><eq/><apply><int/><bvar><ci>y</ci></bvar><apply><divide/><cn type=\"integer\">1</cn><apply><root/><apply><minus/><apply><power/><ci>s</ci><cn type=\"integer\">2</cn></apply><apply><power/><ci>y</ci><cn type=\"integer\">2</cn></apply></apply></apply></apply></apply><apply><plus/><apply><arcsin/><apply><divide/><ci>y</ci><ci>s</ci></apply></apply><ci>c</ci></apply></apply>")
(def hard-ibm-smart-3 "<apply><eq/><apply><int/><bvar><ci>x</ci></bvar><apply><divide/><cn type=\"integer\">1</cn><apply><root/><apply><minus/><apply><power/><ci>l</ci><cn type=\"integer\">2</cn></apply><apply><power/><ci>x</ci><cn type=\"integer\">2</cn></apply></apply></apply></apply></apply><apply><plus/><apply><arcsin/><apply><divide/><ci>x</ci><ci>l</ci></apply></apply><ci>c</ci></apply></apply>")
(def hard-espeak-par-3 "<apply><eq/><apply><int/><bvar><ci>z</ci></bvar><apply><divide/><cn type=\"integer\">1</cn><apply><root/><apply><minus/><apply><power/><ci>t</ci><cn type=\"integer\">2</cn></apply><apply><power/><ci>z</ci><cn type=\"integer\">2</cn></apply></apply></apply></apply></apply><apply><plus/><apply><arcsin/><apply><divide/><ci>z</ci><ci>t</ci></apply></apply><ci>c</ci></apply></apply>")

(def hard-ibm-pause-4 "<apply><sum/><bvar><ci>k</ci></bvar><lowlimit><ci>0</ci></lowlimit><uplimit><ci>n</ci></uplimit><apply><times/><apply><divide/><apply><diff/><bvar><ci>x</ci><degree><ci>k</ci></degree></bvar><apply><ci>g</ci><apply><subscript/><ci>x</ci><cn>0</cn></apply></apply></apply><apply><factorial/><ci>k</ci></apply></apply><apply><power/><apply><minus/><ci>x</ci><apply><subscript/><ci>x</ci><cn>0</cn></apply></apply><ci>k</ci></apply></apply></apply>")
(def hard-ibm-par-4 "<apply><sum/><bvar><ci>n</ci></bvar><lowlimit><ci>0</ci></lowlimit><uplimit><ci>l</ci></uplimit><apply><times/><apply><divide/><apply><diff/><bvar><ci>x</ci><degree><ci>n</ci></degree></bvar><apply><ci>f</ci><apply><subscript/><ci>x</ci><cn>0</cn></apply></apply></apply><apply><factorial/><ci>n</ci></apply></apply><apply><power/><apply><minus/><ci>x</ci><apply><subscript/><ci>x</ci><cn>0</cn></apply></apply><ci>n</ci></apply></apply></apply>")
(def hard-ibm-smart-4 "<apply><sum/><bvar><ci>k</ci></bvar><lowlimit><ci>0</ci></lowlimit><uplimit><ci>n</ci></uplimit><apply><times/><apply><divide/><apply><diff/><bvar><ci>y</ci><degree><ci>k</ci></degree></bvar><apply><ci>h</ci><apply><subscript/><ci>y</ci><cn>0</cn></apply></apply></apply><apply><factorial/><ci>k</ci></apply></apply><apply><power/><apply><minus/><ci>y</ci><apply><subscript/><ci>y</ci><cn>0</cn></apply></apply><ci>k</ci></apply></apply></apply>")

; \lim\left(1+\frac{1}{n}\right)^{n}=e
(def hard-ibm-pause-5 "<apply><eq/><apply><limit/><apply><power/><apply><plus/><cn>1</cn><apply><divide/><cn>1</cn><ci>n</ci></apply></apply><ci>n</ci></apply></apply><ci>e</ci></apply>")
(def hard-ibm-par-5 "<apply><eq/><apply><limit/><apply><power/><apply><plus/><cn>1</cn><apply><divide/><cn>1</cn><ci>x</ci></apply></apply><ci>x</ci></apply></apply><ci>e</ci></apply>")
(def hard-ibm-smart-5 "<apply><eq/><apply><limit/><apply><power/><apply><plus/><cn>1</cn><apply><divide/><cn>1</cn><ci>k</ci></apply></apply><ci>k</ci></apply></apply><ci>e</ci></apply>")
(def hard-espeak-smart-5 "<apply><eq/><apply><limit/><apply><power/><apply><plus/><cn>1</cn><apply><divide/><cn>1</cn><ci>t</ci></apply></apply><ci>t</ci></apply></apply><ci>e</ci></apply>")

;;g(x)=(1+x)\left(\frac{1}{x}-1\right)
;(def hard-v1-5 "<apply><eq/><apply><ci>g</ci><ci>x</ci></apply><apply><times/><apply><plus/><cn>1</cn><ci>x</ci></apply><apply><minus/><apply><divide/><cn>1</cn><ci>x</ci></apply><cn>1</cn></apply></apply></apply>")
;(def hard-v2-5 "<apply><eq/><apply><ci>f</ci><ci>y</ci></apply><apply><times/><apply><plus/><cn>1</cn><ci>y</ci></apply><apply><minus/><apply><divide/><cn>1</cn><ci>y</ci></apply><cn>1</cn></apply></apply></apply>")
;(def hard-v3-5 "<apply><eq/><apply><ci>h</ci><ci>z</ci></apply><apply><times/><apply><plus/><cn>1</cn><ci>z</ci></apply><apply><minus/><apply><divide/><cn>1</cn><ci>z</ci></apply><cn>1</cn></apply></apply></apply>")
;(def hard-v4-5 "<apply><eq/><apply><ci>f</ci><ci>x</ci></apply><apply><times/><apply><plus/><cn>1</cn><ci>x</ci></apply><apply><minus/><apply><divide/><cn>1</cn><ci>x</ci></apply><cn>1</cn></apply></apply></apply>")

;\abs{x(t) - y(t)} \leq \int_{0}^{t}\abs{g(s)}\abs{f(x(s)) - f(y(s))}\mbox{d}s
;(def hard-v1-5 "<apply><leq/><apply><abs/><apply><minus/><apply><ci>x</ci><ci>t</ci></apply><apply><ci>y</ci><ci>t</ci></apply></apply></apply><apply><int/><bvar><ci>s</ci></bvar><lowlimit><cn>0</cn></lowlimit><uplimit><ci>t</ci></uplimit><apply><times/><apply><abs/><apply><ci>g</ci><ci>s</ci></apply></apply><apply><abs/><apply><minus/><apply><ci>f</ci><apply><ci>x</ci><ci>s</ci></apply></apply><apply><ci>f</ci><apply><ci>y</ci><ci>s</ci></apply></apply></apply></apply></apply></apply></apply>")
;(def hard-v2-5 "<apply><leq/><apply><abs/><apply><minus/><apply><ci>y</ci><ci>s</ci></apply><apply><ci>z</ci><ci>s</ci></apply></apply></apply><apply><int/><bvar><ci>t</ci></bvar><lowlimit><cn>0</cn></lowlimit><uplimit><ci>s</ci></uplimit><apply><times/><apply><abs/><apply><ci>f</ci><ci>t</ci></apply></apply><apply><abs/><apply><minus/><apply><ci>h</ci><apply><ci>y</ci><ci>t</ci></apply></apply><apply><ci>h</ci><apply><ci>z</ci><ci>t</ci></apply></apply></apply></apply></apply></apply></apply>")
;(def hard-v3-5 "<apply><leq/><apply><abs/><apply><minus/><apply><ci>z</ci><ci>b</ci></apply><apply><ci>x</ci><ci>b</ci></apply></apply></apply><apply><int/><bvar><ci>c</ci></bvar><lowlimit><cn>0</cn></lowlimit><uplimit><ci>b</ci></uplimit><apply><times/><apply><abs/><apply><ci>h</ci><ci>c</ci></apply></apply><apply><abs/><apply><minus/><apply><ci>f</ci><apply><ci>z</ci><ci>c</ci></apply></apply><apply><ci>f</ci><apply><ci>x</ci><ci>c</ci></apply></apply></apply></apply></apply></apply></apply>")
;(def hard-v4-5 "<apply><leq/><apply><abs/><apply><minus/><apply><ci>x</ci><ci>c</ci></apply><apply><ci>y</ci><ci>c</ci></apply></apply></apply><apply><int/><bvar><ci>b</ci></bvar><lowlimit><cn>0</cn></lowlimit><uplimit><ci>c</ci></uplimit><apply><times/><apply><abs/><apply><ci>f</ci><ci>b</ci></apply></apply><apply><abs/><apply><minus/><apply><ci>g</ci><apply><ci>x</ci><ci>b</ci></apply></apply><apply><ci>g</ci><apply><ci>y</ci><ci>b</ci></apply></apply></apply></apply></apply></apply></apply>")

;\tan{(b-c)} = \frac{\tan{b} - \tan{c}}{1 + (\tan{b})(\tan{c})}
;(def hard-v1-5 "<apply><eq/><apply><tan/><apply><minus/><ci>b</ci><ci>c</ci></apply></apply><apply><divide/><apply><minus/><apply><tan/><ci>b</ci></apply><apply><tan/><ci>c</ci></apply></apply><apply><plus/><cn>1</cn><apply><times/><apply><tan/><ci>b</ci></apply><apply><tan/><ci>c</ci></apply></apply></apply></apply></apply>")
;(def hard-v2-5 "")
;(def hard-v3-5 "")
;(def hard-v4-5 "")

(defn write-on-disk [dir index name xml]
  (with-open [w (clojure.java.io/writer  (str dir index "_" name ".xml"))]
    (.write w (str xml))))

;(write-on-disk "/home/alephzero/Git/TesiMagistrale/Statistics/Result/correct/" "25" "hard-espeak-smart-5" hard-espeak-smart-5)

;21,4 MB
(ns application.sperimentation-analyser
  (:require [application.statistics :as stat]
            [clojure.java.io :as io])
  (:import (java.io StringReader BufferedReader))
  (use [clojure.algo.generic.functor :only (fmap)]))

(def ^:const EXPERIMENT_USERS ["1_user" "2_user" "3_user" "4_user"])
(def ^:const CORRECT "correct")
(def ^:const EXPERIMENT_FOLDER_PATH "../../data_dev/statistic_data/")
(def ^:const RESULT_FOLDER_PATH "../../data_dev/statistic_data/result/")

(defn get-files-path [folder-path & [filter-function]]
  (let [filter-function (if filter-function filter-function (fn [_] true))]
    (sort (map str (filter #(and (.isFile %) (filter-function (.getName %))) (file-seq (io/file folder-path)))))))

(defn extract-file-number [file-path]
  (Integer. (re-find #"(?<=/)[^/]\d" file-path)))

(defn compute-distance [score-function exp-path act-path]
  (let [exp-cmml (slurp exp-path)
        act-cmml (slurp act-path)
        file-number (extract-file-number exp-path)
        score (score-function exp-cmml act-cmml)]
    {file-number score}))

(defn write-score-on-disk [score-list destination-path]
  (doall                                                    ; score-list is a lazy-seq. doall is needed to force evaluation
    (map-indexed
      (fn [index value] (spit destination-path (str (+ index 1) " " value "\n") :append true))
      score-list)))

(defn collect-data [user score-function post-fiss  & [filter-function]]
  (let [destination-path (str RESULT_FOLDER_PATH "result_" user "_" post-fiss ".txt")
        correct-paths (get-files-path (str EXPERIMENT_FOLDER_PATH CORRECT) filter-function)
        user-paths (get-files-path (str EXPERIMENT_FOLDER_PATH user) filter-function)
        score-list (map (partial compute-distance score-function) correct-paths user-paths)]
    (doseq [x score-list]
      (spit destination-path (str (apply key x) " " (apply val x) "\n") :append true))
    score-list))

(defn collect-data-all-user [users score-function destination-path & [filter-function]]
  (map #(collect-data % score-function destination-path filter-function) users ))


(defn str->map [s]
  (let [key (re-find #".+(?=\s)" s)
        value (re-find #"(?<=\s).+" s)]
    {(Integer. key) (Float. value)}))

(defn process-reader [rdr]
  (let [xs (line-seq rdr)]
    (into (sorted-map) (map str->map xs))))

(defn foo [x]
  (/ x 1))
;(defn foo [x]
;  x)

(defn compute-average-from-files [result-file-names destination-path]
  (let [score-list
        (->> RESULT_FOLDER_PATH
             io/file
             file-seq
             (filter #(and (.isFile %) (contains? result-file-names (.getName %))))
             (map #(-> % io/reader process-reader))
             (apply (partial merge-with +))
             (fmap #(/ % (count result-file-names)))
             )]
    (run! println score-list)
    ;(write-score-on-disk score-list destination-path)
    (doseq [x score-list]
      ;(spit destination-path (str (apply key x) " " (apply val x) "\n") :append true)
      (spit destination-path (str (key x) " " (val x)  "\n") :append true)
      )
    )
  )

(def ^:const ALL_TEST (hash-set "result_user_1_spice.txt" "result_user_2_spice.txt" "result_user_3_spice.txt" "result_user_4_spice.txt"))
(def ^:const ONLY_ESPEAK (hash-set "result_user_1_spice_only_espeak.txt" "result_user_2_spice_only_espeak.txt"
                                   "result_user_3_spice_only_espeak.txt" "result_user_4_spice_only_espeak.txt"))
(def ^:const IBM_OVER_ESPEAK_PAUSE (hash-set "result_user_1_spice_ibm_over_espeak_pause.txt"
                                             "result_user_2_spice_ibm_over_espeak_pause.txt"
                                             "result_user_3_spice_ibm_over_espeak_pause.txt"
                                             "result_user_4_spice_ibm_over_espeak_pause.txt"))

(def ^:const IBM_OVER_ESPEAK_PAR (hash-set "result_user_1_spice_ibm_over_espeak_par.txt"
                                             "result_user_2_spice_ibm_over_espeak_par.txt"
                                             "result_user_3_spice_ibm_over_espeak_par.txt"
                                             "result_user_4_spice_ibm_over_espeak_par.txt"))

(def ^:const IBM_OVER_ESPEAK_SMART (hash-set "result_user_1_spice_ibm_over_espeak_smart.txt"
                                             "result_user_2_spice_ibm_over_espeak_smart.txt"
                                             "result_user_3_spice_ibm_over_espeak_smart.txt"
                                             "result_user_4_spice_ibm_over_espeak_smart.txt"))

(def ^:const IBM_OVER_ESPEAK_ALL_STRATEGY (hash-set "result_all_user_average_spice_ibm_over_espeak_pause.txt"
                                                    "result_all_user_average_spice_ibm_over_espeak_par.txt"
                                                    "result_all_user_average_spice_ibm_over_espeak_smart.txt"))

(def ^:const ALL_EXCEPT_FAMOUS (hash-set "result_user_1_spice_all_except_famous.txt"
                                         "result_user_2_spice_all_except_famous.txt"
                                         "result_user_3_spice_all_except_famous.txt"
                                         "result_user_4_spice_all_except_famous.txt"))

(def ^:const ALL_USERS ["user_1" "user_2" "user_3" "user_4"])



; Filter file name
(defn filter-function-only-espeak [x]
  (re-find #"espeak" x))
(defn filter-function-only-ibm [x]
  (re-find #"ibm" x))


; Filter file name
(defn filter-function-ibm-over-espeak-pause [x]
  (re-find #"01|04|08|10|12" x))
(defn filter-function-ibm-over-espeak-par [x]
  (re-find #"01|04|13|15|17" x))
(defn filter-function-ibm-over-espeak-smart [x]
  (re-find #"01|04|18|20|22" x))

(defn filter-function-easy [x]
  (re-find #"easy" x))
(defn filter-function-hard [x]
  (re-find #"hard" x))

(defn filter-function-par [x]
  (re-find #"par" x))
(defn filter-function-pause [x]
  (re-find #"pause" x))
(defn filter-function-smart [x]
  (re-find #"smart" x))


(defn filter-function-all-except-famous [x]
  (re-find #"01|02|03|04|05|06|07|08|09|10|13|14|15|18|19|20|23|24" x))
(defn filter-function-only-famous [x]
  (re-find #"11|12|16|17|21|22|25" x))

(run! println (collect-data-all-user ALL_USERS stat/spice  "spice_only_ibm" filter-function-only-ibm))

;(compute-average-from-files ALL_EXCEPT_FAMOUS (str RESULT_FOLDER_PATH "result_all_user_average_spice_all_except_famous.txt"))


;This file is part of formula-to-speech.
;
;Formula-to-speech is free software: you can redistribute it and/or modify
;it under the terms of the GNU General Public License as published by
;the Free Software Foundation, either version 3 of the License, or
;(at your option) any later version.
;
;Formula-to-speech is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with formula-to-speech.  If not, see <https://www.gnu.org/licenses/>.

(ns application.statistics
  (:require [clojure.zip :as zip]
            [clojure.xml :as xml]
            [clojure.set :as set]
            [examples.example :as ex]
            [clojure.string :as str])
  (:import (java.io ByteArrayInputStream)
           (at.unisalzburg.dbresearch.apted.parser BracketStringInputParser)
           (at.unisalzburg.dbresearch.apted.distance APTED)
           (at.unisalzburg.dbresearch.apted.costmodel StringUnitCostModel)))


(def ^:const EDGE_CHILD_NAMES {1 :first 2 :second 3 :third 4 :fourth})

(defn xml->zipper [xml]
  (-> xml
      .getBytes
      ByteArrayInputStream.
      xml/parse
      zip/xml-zip))

(defn extract-value [loc]
  (if (zip/branch? loc)
    (let [tag (-> loc zip/node :tag)]
      (when (and(not= tag :bvar) (not= tag :ci)(not= tag :cn))
        tag))
    (zip/node loc)))

(defn process-node [loc]
  (let [arity (-> loc zip/children count)]
    (loop [loc loc
           edges []
           index 1]
      (if (< index arity)
        (let [operator (-> loc zip/down zip/node :tag)
              operand-node (-> loc zip/children (nth index))
              operand (if (= (operand-node :tag) :apply)
                        (-> operand-node :content first :tag)
                        (-> operand-node :content first))
              edge {:operator operator (get EDGE_CHILD_NAMES index) operand}]
          (recur loc (conj edges edge) (+ index 1)))
        edges))))

(defn generate-scene-graph [loc]
  (loop [loc loc
         scene-graph []]
    (if (-> loc zip/end?)
      (do (zip/root loc)
          scene-graph)
      (if (= (-> loc zip/node :tag) :apply)
        (let [edges (process-node loc)
              scene-graph (concat scene-graph edges)]
          (recur (zip/next loc) scene-graph))
        (let [value (extract-value loc)]
          (if (nil? value)
            (recur (zip/next loc) scene-graph)
            (recur (zip/next loc) (conj scene-graph {:value value}))))))))

(defn jaccard-index [expected actual]
  (let [intersection (set/intersection expected actual)
        union (set/union expected actual)]
    (println (str "- attesi: " expected))
    (println (str "- attuali: " actual))
    (println (str "- intersezione: " intersection))
    (println (str "- unione: " union))
    (println (str "- indice di Jaccard: " (float (/ (count intersection) (count union)))))))

(defn drop-first [x xs]
  (let [head (first xs)
        tail (rest xs)]
    (when (not-empty xs)
      (if (= head x)
        tail
        (cons head (drop-first x tail))))))

(defn seq-intersect-count [xs ys]
  (let [joined-seqs (concat xs ys)]
    (reduce + (map  (fn [x] (-> x second (quot 2)))(frequencies joined-seqs)))))

(defn compute-precision [exp act]
  (float (/ (seq-intersect-count exp act) (count exp))))

(defn compute-recall [exp act]
  (float (/ (seq-intersect-count exp act) (count act))))

(defn spice [exp act & [info]]
  (let [exp-graph (-> exp xml->zipper generate-scene-graph)
        act-graph (-> act xml->zipper generate-scene-graph)
        precision (compute-precision exp-graph act-graph)
        recall (compute-recall exp-graph act-graph)
        score (float (/ (* 2 precision recall) (+ precision recall)))]
    (if info
      (do
        (println "SPICE SCORE")
        (println (str " - Recall: " recall))
        (println (str " - Precision: " precision))
        (println (str " - Score: " score "\n"))
        score)
      score)))

(defn cmml->bracket-notation [loc]
  (if (zip/end? loc)
    (do
      (zip/root loc)
      "")
    (if (= (-> loc zip/node :tag) :apply)
      (let [operator (-> loc zip/down zip/node :tag name)
            operands (-> loc zip/children rest)]
        (str "{" operator (str/join (map #(-> % zip/xml-zip cmml->bracket-notation) operands)) "}"))
      (str "{" (-> loc zip/down zip/node )  "}"))))

(defn node-count-on-bracket [bracket-string]
  (count(filter #(= % \}) bracket-string)))

(defn edit-tree-distance [cmml1 cmml2 & [info]]
  (let [bracket-string-1 (-> cmml1 xml->zipper cmml->bracket-notation)
        bracket-string-2 (-> cmml2 xml->zipper cmml->bracket-notation)
        parser (BracketStringInputParser.)
        t1 (. parser fromString bracket-string-1)
        t2 (. parser fromString bracket-string-2)
        apted (new APTED (StringUnitCostModel.))
        distance (. apted computeEditDistance t1 t2)
        max-count (max (node-count-on-bracket bracket-string-1) (node-count-on-bracket bracket-string-2))
        normalized-score (- 1 (float (/ distance max-count)))]
    (if info
      (do
        (println (str "EDIT TREE DISTANCE"))
        (println (str " - Edit distance:" distance))
        (println (str " - Max node count:" max-count))
        (println (str " - Normalized score: " normalized-score "\n"))
        normalized-score)
      normalized-score)))

(def dup1 "<apply><eq/><apply><plus/><ci>a</ci><cn>1</cn></apply><apply><plus/><ci>b</ci><cn>1</cn></apply></apply>")
(def dup2 "<apply><eq/><apply><plus/><ci>x</ci><cn>1</cn></apply><apply><plus/><ci>b</ci><cn>3</cn></apply></apply>")
(def act-hard-v1-3 "<apply><eq/><apply><int/><bvar><ci>x</ci></bvar><apply><divide/><cn type=\"integer\">1</cn><apply><root/><apply><minus/><apply><power/><ci>k</ci><cn type=\"integer\">2</cn></apply><apply><power/><ci>x</ci><cn type=\"integer\">2</cn></apply></apply></apply></apply></apply><apply><arcsin/><apply><plus/><apply><div/><ci>x</ci><ci>k</ci></apply><ci>c</ci></apply></apply></apply>")
(def exp-hard-v1-3 ex/hard-ibm-pause-3)
(def r "<apply><eq/><ci>y</ci><apply><plus/><ci>x</ci><cn>1</cn></apply></apply>")
(def w "<apply><eq/><ci>y</ci><cn>1</cn></apply>")

;(edit-distance-tree dup1 dup2 true)
;(spice dup1 dup2 true)

;(edit-distance-tree exp-hard-v1-3 act-hard-v1-3 true)
;(spice exp-hard-v1-3 act-hard-v1-3 true)
;
;(edit-tree-distance r w true)
;(spice r w true)

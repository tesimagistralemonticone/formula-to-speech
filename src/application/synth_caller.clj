;This file is part of formula-to-speech.
;
;Formula-to-speech is free software: you can redistribute it and/or modify
;it under the terms of the GNU General Public License as published by
;the Free Software Foundation, either version 3 of the License, or
;(at your option) any later version.
;
;Formula-to-speech is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with formula-to-speech.  If not, see <https://www.gnu.org/licenses/>.

(ns application.synth-caller
  (:require [clojure.string :as string]
            [clojure.java.io :as io]
            [util.config-manager :as conf]
            )
  (:import (com.ibm.watson.developer_cloud.service.security IamOptions IamOptions$Builder)
           (com.ibm.watson.developer_cloud.text_to_speech.v1 TextToSpeech)
           (com.ibm.watson.developer_cloud.text_to_speech.v1.model SynthesizeOptions$Builder)
           (com.ibm.watson.developer_cloud.text_to_speech.v1.util WaveUtils)
           (javax.sound.sampled AudioSystem)
           (javax.swing JOptionPane)
           (java.lang Runtime)))

;(def ^:const APIKEY "3niJEFcDr4y8ChpNHjxxnjVnLlcKrMylRcjCzE9P8MkA")
;(def ^:const URL "https://gateway-lon.watsonplatform.net/text-to-speech/api")
;(def ^:const APIKEY "T5SvgI9luCYgsN1jDmWOBQk79Ua6s0wdAlZLx90xWDWx")
(def ^:const APIKEY (conf/configuration :apikey))
(def ^:const URL "https://gateway-lon.watsonplatform.net/text-to-speech/api")

(def ^:const SYNTHESIZER {:ibm 0 :espeak 1})

(def sound-dir (conf/configuration :audio_dest))

;(def current_path (System/getProperty "user.dir"))
;(def sound-dir (str current_path "/../../data_dev/sounds/"))

(defn connect []
  (let [builder (doto (IamOptions$Builder.) (. apiKey APIKEY))
        options (. builder build)
        text-to-speech (doto (new TextToSpeech options) (. setEndPoint URL))
        ]
    text-to-speech)
)

; Esegue la richiesta al servizio di IBM e ottiene il file audio come input stream
(defn exec-synt-request [text-to-speech sentence]
  (let [builder (doto
                  (SynthesizeOptions$Builder.)
                  (. text sentence)
                  (. accept "audio/wav")
                  (. voice "it-IT_FrancescaV2Voice"))
        options (. builder build)
        caller (. text-to-speech synthesize options)
        input-stream (. caller execute)]
    (WaveUtils/reWriteWaveHeader input-stream)))

(defn save [input-stream dest-name]
  (io/copy input-stream (io/writer (str sound-dir dest-name))))

(defn play-sound [input-stream]
  (let [sound (AudioSystem/getAudioInputStream input-stream)]
    (doto (AudioSystem/getClip) (. open sound) (. start))))

(defn say_and_or_save [sentence synthesizer say? dest]
   (if (= synthesizer (SYNTHESIZER :ibm))
      (let [text-to-speech (connect)
            input-stream (exec-synt-request text-to-speech sentence)]
        (if say? (play-sound input-stream))
        (if (seq dest) (save input-stream dest))
        (JOptionPane/showMessageDialog nil "Termina il thread"))
      (do
        (if (seq dest) (. (Runtime/getRuntime) exec (into-array ["espeak" "-m" "-v" "it" sentence "-w" dest])))
        (if say?(. (Runtime/getRuntime) exec (into-array ["espeak" "-m" "-v" "it" sentence])))
        )
   )
  )




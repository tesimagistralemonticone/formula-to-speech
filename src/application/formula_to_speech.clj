;This file is part of formula-to-speech.
;
;Formula-to-speech is free software: you can redistribute it and/or modify
;it under the terms of the GNU General Public License as published by
;the Free Software Foundation, either version 3 of the License, or
;(at your option) any later version.
;
;Formula-to-speech is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with formula-to-speech.  If not, see <https://www.gnu.org/licenses/>.

(ns application.formula-to-speech
  (:require [clojure.java.io :as io]
            [clojure.xml :as xml]
            [clojure.zip :as zip]
            [util.number-converter :as converter]
            [examples.example :as ex]
            [util.dictionary-manager :as dic]
            [util.config-manager :as conf]
            [application.synth-caller :as synth-caller]
            [clojure.core.match :refer [match]]
            [clojure.string :as str]
            [clojure.string :as string])
  (:use [clojure.pprint])
  (:import (simplenlg.lexicon.italian ITXMLLexicon)
           (simplenlg.framework NLGFactory Language)
           (simplenlg.realiser Realiser)
           (java.io ByteArrayInputStream File)
           (simplenlg.features Feature InternalFeature Form)
           (simplenlg.lexicon MultipleLexicon)))

(def operator-dic (dic/load-operator-dic (dic/DICTIONARY_PATH :operators-it)))

(def ^:const AGGREGATION_METHOD {:pause 0 :parenthesis 1 :smart 2})


(def lex-ita (ITXMLLexicon.))

; Expand the resource file in a physic file. Required when the program runs from jar format. Note: remember to delete it
(defn expand-math-italian-lex-file []
  (let [temp-file (File/createTempFile "math_it_lex" ".xml")]
    (spit temp-file (-> "lexicon/math-italian-lexicon.xml" io/resource slurp))
    temp-file))

;(def lex-math-ita (ITXMLLexicon. (.getPath (io/resource "lexicon/math-italian-lexicon.xml"))))
(def lex-math-ita
  (let  [math-italian-lex-file (expand-math-italian-lex-file)
         xml-lex (ITXMLLexicon. math-italian-lex-file)]
    (.delete math-italian-lex-file)
    xml-lex))

(def multi-lex-ita
  (doto (MultipleLexicon. Language/ITALIAN)
    (. addInitialLexicon lex-ita)
    (.  addFinalLexicon lex-math-ita)))

(def nlg-factory (NLGFactory. multi-lex-ita))

(def realiser (Realiser.))

(def it-determiner "il")


(def pause (str "<break time=\"" (conf/configuration :pause-length) "ms\"/>"))

(def ^:const LIMIT_TYPE {:default 0 :alternative 1})

(defn xml->zipper [xml]
  (-> xml
      .getBytes
      ByteArrayInputStream.
      xml/parse
      zip/xml-zip
      zip/root))

; Alias per (seq word). Siccome (empty? word) è implementata come (not (seq word)), risulta che (not (empty? word)
; equivale a (not (not (seq word)). Dunque è consigliato usare (seq word) al posto di (not (not (seq word)).
(defn not-empty? [word]
  (seq word))

(def not-nil? (comp not nil?))


(defn generate-noun-phrase [det? noun]
  (let [trimmed-noun (str/trim noun)]
    (if det?
      (. nlg-factory createNounPhrase it-determiner trimmed-noun)
      (. nlg-factory createNounPhrase trimmed-noun))))


(defn generate-prepositional-phrase [prep sentence]
  (let [pp (. nlg-factory createPrepositionPhrase prep sentence)]
    pp))


(declare generate-text)
(declare generate-unary-clause)
(declare generate-operand)


(defn generate-limit [operator-node aggregation-method & [limit-type]]
  (let [operator (operator-dic (operator-node :tag))
        prep (if (= limit-type (LIMIT_TYPE :alternative)) (operator :preposition-alt) (operator :preposition))
        operand (generate-operand (-> operator-node :content first) aggregation-method)
        pp (generate-prepositional-phrase prep operand)]
    pp))


(defn wrap-phrase [phrase node-children salience-parent aggregation-method]
  (if (= (node-children :tag) :apply)
    (let [operator-node (-> node-children :content first)
          salience-children (-> (operator-dic (operator-node :tag)) :salience)]
      (if (and (not-nil? salience-children) (not-nil? salience-parent) (< salience-parent salience-children))
        (if (= aggregation-method (AGGREGATION_METHOD :parenthesis))
          (let [wrap (generate-noun-phrase false "(")]
            (doto wrap (. addComplement phrase) (. addPostModifier ")")))
          (let [wrap (generate-noun-phrase false pause)]
            (doto wrap (. addComplement phrase) (. addPostModifier pause))))
        phrase))
    phrase))


(defn generate-degree [operand-node]
  (let [degree-ordinal (-> operand-node :content first)]
    (if (not-nil? degree-ordinal)
      (if (= (operand-node :tag) :cn)
        (converter/ordinal->cardinal (Integer/parseInt degree-ordinal))
        (str degree-ordinal "-esima"))
      nil)))


(defn look-for-degree [operand-node]
  (let [degree-ordinal (generate-degree (-> operand-node :content second :content first))]
    degree-ordinal
    ))


(defn generate-operand [operand-node aggregation-method & [det?]]
  (match [(operand-node :tag)]
         [:ci] (generate-noun-phrase det? (-> operand-node :content first))
         [:cn] (generate-noun-phrase det? (-> operand-node :content first))
         [:bvar] (generate-operand (-> operand-node :content first) aggregation-method false)
         [:degree] (generate-degree (-> operand-node :content first))
         [:apply] (generate-text operand-node aggregation-method)
         [:bind] (generate-text operand-node aggregation-method)
         :else (println (str "operand not found: " operand-node))))


; Genera l'oggetto. Potrebbe essere un aggettivo (es maggiore in "x è maggiore di 3") oppure una coordinazione di
; aggettivi (es maggiore o ugualle  in "x è maggiore o uguale a 3")
(defn generate-object [operator]
  (let [coord-name (operator :coordinated-name)
        pre-modifier (operator :pre-modifier)
        adj1 (. nlg-factory createAdjectivePhrase (operator :name))]
    (cond
      (not-empty? coord-name)
      (let [adj2 (. nlg-factory createAdjectivePhrase coord-name)]
        (doto
          (. nlg-factory createCoordinatedPhrase)
          (. addCoordinate adj1)
          (. addCoordinate adj2)
          (. setConjunction (operator :coordination-conj))))
      (not-empty? pre-modifier)
      (let [adv (. nlg-factory createAdverbPhrase pre-modifier)]
        (doto
          adj1
          (. addPreModifier adv)))
      :else adj1)))


(defn generate-unary-clause [operator-node operand-node aggregation-method]
  (let [operator-tag (operator-node :tag)
        ; Caso speciale, l'operatore è una funzione e non ha un tag specifico associato, solo un generico :ci.
        ; Gli si assegna quindi manualmente il tag :function
        operator-tag (if (= operator-tag :ci) :function operator-tag)
        operator (operator-dic operator-tag)
        operand (generate-operand operand-node aggregation-method)
        salience (operator :salience)]
    (match [(operator :category)]
           ;    -3 , +4, etc...
           ["arithmetic-algebra-op"]
           (let [adj (. nlg-factory createAdjectivePhrase (operator :name))]
             (doto operand (. addPreModifier adj)))
           ;    abs{x}
           ["basic-content-elements"]
           (let [np (generate-noun-phrase false (operator :name))
                 adj (. nlg-factory createAdjectivePhrase (operator :post-modifier))
                 pp (generate-prepositional-phrase (operator :preposition) np)]
             (doto np (. addPostModifier adj))
             (doto pp (. addPostModifier operand) )
             )
           ; sin, radice quadrata (quadrata sottointesa)
           ["elementary-classical-functions"]
           (let [np (generate-noun-phrase (operator :determiner) (operator :name))
                 operand (generate-operand operand-node aggregation-method)
                 aggr-op (wrap-phrase operand operand-node salience aggregation-method)
                 pp (generate-prepositional-phrase (operator :preposition) aggr-op)
                 post-modifier (operator :post-modifier)
                 ]
             (when (not-empty? post-modifier)
               (let [adj (. nlg-factory createAdjectivePhrase post-modifier)]
                 (. np addComplement adj)))
             (doto np (. addComplement pp)))
           ["function-id"]
           (let [function-name (-> operator-node :content first)
                 np (generate-noun-phrase (operator :determiner) function-name)
                 operand (generate-operand operand-node aggregation-method)
                 aggr-operand (wrap-phrase operand operand-node salience aggregation-method)
                 pp (generate-prepositional-phrase (operator :preposition) aggr-operand)]
             (doto np (. addComplement pp)))
           ["sequence"]
           (let [subj (generate-noun-phrase (operator :determiner) (operator :name))
                 operand (generate-operand operand-node aggregation-method)
                 aggr-operand (wrap-phrase operand operand-node salience aggregation-method)
                 pp (generate-prepositional-phrase (operator :preposition) aggr-operand)]
             (doto subj (. addComplement pp)))

           ["calculus"]
           (let [subj (generate-noun-phrase (operator :determiner) (operator :name))
                 operand (generate-operand operand-node aggregation-method)
                 pp (generate-prepositional-phrase (operator :preposition) operand)]
             (doto subj (. addComplement pp)))
           :else (println (str (operator :category) ": categoria non conosciuta"))
           )
    ))

(defn generate-binary-clause [operator-node operand1-node operand2-node aggregation-method]
  (let [operator (operator-dic (operator-node :tag))
        salience (operator :salience)]
    (match [(operator :category)]
           ["rel-op"]
           (let [vp (. nlg-factory createVerbPhrase (operator :verb))
                 obj (generate-object operator)
                 op1 (generate-operand operand1-node aggregation-method)
                 op2 (generate-operand operand2-node aggregation-method)
                 pp (generate-prepositional-phrase (operator :preposition) op2)
                 s (. nlg-factory createClause op1 vp obj)]
             (doto s
               (. setFeature Feature/NEGATED (operator :is-negate))
               (. setFeature Feature/SUPRESSED_COMPLEMENTISER true)
               (. addPostModifier pp))
             s)
           ["arithmetic-algebra-op"]
           (let [vp (. nlg-factory createVerbPhrase (operator :name))
                 op1 (generate-operand operand1-node aggregation-method)
                 op2 (generate-operand operand2-node aggregation-method)
                 aggr-op1 (wrap-phrase op1 operand1-node salience aggregation-method)
                 aggr-op2 (wrap-phrase op2 operand2-node salience aggregation-method)
                 prep (operator :preposition)
                 s (. nlg-factory createClause aggr-op1 vp aggr-op2)]
             (if (not-empty? prep)
               (let [pp (generate-prepositional-phrase prep aggr-op2)]
                 (doto s (. setObject pp)))
               (doto s (. setObject aggr-op2)))
             (when (operator :is-past-participle-form)
               (. s setFeature Feature/FORM Form/PAST_PARTICIPLE))
             (doto s (.  setFeature Feature/NEGATED (operator :is-negate))
                     (. setFeature Feature/SUPRESSED_COMPLEMENTISER true)))
           ["logic-op"]
           (let [op1 (generate-operand operand1-node aggregation-method)
                 op2 (generate-operand operand2-node aggregation-method)
                 coord (. nlg-factory createCoordinatedPhrase)
                 main-conj (operator :name)
                 pre-conj (operator :pre-conj)]
             (if (not-empty pre-conj)
               (let [pp1 (generate-prepositional-phrase pre-conj op1)]
                 (. coord addCoordinate pp1)
                 (. op1 setFeature Feature/SUPRESSED_COMPLEMENTISER true))
               (. coord addCoordinate op1))
             (doto coord
               (. addCoordinate op2)
               (. setConjunction main-conj)))

           ["conditional-set"]
           (let [obj (generate-noun-phrase true (operator :name))
                 op1 (generate-operand operand1-node aggregation-method true)
                 op2 (generate-operand operand2-node aggregation-method true)
                 pp (generate-prepositional-phrase (operator :preposition) op1)
                 pp2 (generate-prepositional-phrase "tali che" op2)] ;TODO capire come gestire il "tale che"
             (. op1 setPlural true)
             (. op2 removeFeature InternalFeature/CLAUSE_STATUS)
             (doto
               obj
               (. addComplement pp)
               (. addComplement pp2)))
           ["basic-content-elements"]
           (let [subj (generate-noun-phrase true (operator :name))
                 op1 (generate-operand operand1-node aggregation-method)
                 op2 (generate-operand operand2-node aggregation-method)
                 coord (. nlg-factory createCoordinatedPhrase)
                 pp (generate-prepositional-phrase (operator :preposition) coord)]
             (doto coord
               (. addCoordinate op1)
               (. addCoordinate op2))
             (doto subj (. addComplement pp)))
           ["elementary-classical-functions"]
           (let [np (generate-noun-phrase (operator :determiner) (operator :name))
                 op1 (generate-operand operand1-node aggregation-method)
                 op2 (generate-operand operand2-node aggregation-method) ;degree
                 pp (generate-prepositional-phrase (operator :preposition) op2)
                 ]
             (doto np (. addComplement op1)(. addComplement pp)))
           ["logic-quantifiers"]
           (let [np (generate-noun-phrase (operator :determiner) (operator :name))
                 op1 (generate-operand operand1-node aggregation-method)
                 op2 (generate-operand operand2-node aggregation-method) ;degree
                 pp (generate-prepositional-phrase (operator :preposition) op2)
                 ]
             (doto np (. addComplement op1)(. addComplement pp)))
           ["calculus"]
           (let [subj (generate-noun-phrase (operator :determiner) (operator :name)) ; l'integrale
                 func (generate-operand operand2-node aggregation-method)          ;f di x
                 pp-func (generate-prepositional-phrase (operator :preposition) func) ; di f di x
                 bound-var (generate-operand operand1-node aggregation-method)          ; x
                 degree (look-for-degree operand1-node)
                 pp-bound-var (generate-prepositional-phrase (operator :sub-preposition) bound-var) ; in x
                 ]
             (when (not-nil? degree)
               (let [adj (. nlg-factory createAdjectivePhrase degree)]
                 (. subj addComplement adj)))
             (. bound-var addPreModifier (operator :pre-modifier)) ; rispetto x (derivate)
             (doto subj
               (. addComplement pp-func)
               (. addComplement pp-bound-var)
               ))
           ["aux"]
           (let [subj (generate-operand operand1-node aggregation-method)
                 obj (generate-operand operand2-node aggregation-method)
                 pp (generate-prepositional-phrase (operator :preposition) obj)]
             (doto subj (. addComplement pp)))
           :else (println (str (operator :category) ": categoria non conosciuta")))))


(defn generate-ternary-clause [operator-node operand1-node operand2-node operand3-node aggregation-method]
  (let [operator (operator-dic (operator-node :tag))
        salience (operator :salience)]
    (match [(operator :category)]
           ;     Es: il limite per x tendente a zero di f di x
           ["sequence"]
           (let [sub-subj (generate-operand operand1-node aggregation-method)    ;x
                 sub-verb (. nlg-factory createVerbPhrase (operator :verb)) ; tendere
                 sub-obj (generate-limit operand2-node aggregation-method (LIMIT_TYPE :alternative))         ;a zero
                 sub-sentence (. nlg-factory createClause sub-subj sub-verb sub-obj) ; x tendere a zero

                 main-sentence (generate-noun-phrase true (operator :name)) ; il limite
                 sub-pp (generate-prepositional-phrase (operator :sub-preposition) sub-sentence)
                 op3 (generate-operand operand3-node aggregation-method)
                 aggr-op3 (wrap-phrase op3 operand3-node salience aggregation-method)
                 main-pp (generate-prepositional-phrase (operator :preposition) aggr-op3)]
             (. sub-verb setFeature Feature/FORM Form/PRESENT_PARTICIPLE) ; tendere -> tendente
             (. sub-sentence removeFeature InternalFeature/CLAUSE_STATUS)
             (. op3 removeFeature InternalFeature/CLAUSE_STATUS)
             (doto main-sentence (. addComplement sub-pp) (. addComplement main-pp)))
           :else (println (str (operator :category) ": categoria non conosciuta")))))


; Es: sommatoria per x da 0 a 5 di seno di x
(defn generate-quaternary-clause [operator-node operand1-node operand2-node operand3-node operand4-node aggregation-method]
  (let [operator (operator-dic (operator-node :tag))
        salience (operator :salience)]
    (match [(operator :category)]
           ["sequence"]
           (let [np (generate-noun-phrase (operator :determiner) (operator :name)) ;la sommatoria
                 op1 (generate-operand operand1-node aggregation-method)          ; x
                 pp1 (generate-prepositional-phrase (operator :sub-preposition) op1) ; per x
                 low-lim (generate-limit operand2-node aggregation-method)            ; da 0
                 up-lim (generate-limit operand3-node aggregation-method)            ; a 5
                 op4 (generate-operand operand4-node aggregation-method)          ;seno di x
                 aggr-op4 (wrap-phrase op4 operand4-node salience aggregation-method)

                 pp4 (generate-prepositional-phrase (operator :preposition) aggr-op4)]
             (doto np
               (. addComplement pp1)
               (. addComplement low-lim)
               (. addComplement up-lim)
               (. addComplement pp4)))
           ["calculus"]
           (let [subj (generate-noun-phrase (operator :determiner) (operator :name)) ; l'integrale
                 from (generate-limit operand2-node aggregation-method)            ; da 0
                 to (generate-limit operand3-node aggregation-method)            ; a n
                 func (generate-operand operand4-node aggregation-method)          ;f di x
                 pp-func (generate-prepositional-phrase (operator :preposition) func) ; di f di x
                 bound-var (generate-operand operand1-node aggregation-method)          ; x
                 pp-bound-var (generate-prepositional-phrase (operator :sub-preposition) bound-var) ; in x
                 ]
             (. bound-var addPreModifier (operator :pre-modifier))
             (doto subj
               (. addComplement from)
               (. addComplement to)
               (. addComplement pp-func)
               (. addComplement pp-bound-var)
               ))
           :else (println "categoria non conosciuta"))));


(defn generate-text [node aggregation-method]
  (let [loc (zip/xml-zip node)
        arity (-> loc zip/children count)
        operator (-> loc zip/down zip/node)
        operand1 (-> loc zip/down zip/right zip/node)]
    (cond
      (= arity 2)
      (let [clause (generate-unary-clause operator operand1 aggregation-method)]
        clause)
      (= arity 3)
      (let [operand2 (-> loc zip/down zip/right zip/right zip/node)
            clause (generate-binary-clause operator operand1 operand2 aggregation-method)]
        clause)
      (= arity 4)
      (let [operand2 (-> loc zip/down zip/right zip/right zip/node)
            operand3 (-> loc zip/down zip/right zip/right zip/right zip/node)
            clause (generate-ternary-clause operator operand1 operand2 operand3 aggregation-method)]
        clause)
      (= arity 5)
      (let [operand2 (-> loc zip/down zip/right zip/right zip/node)
            operand3 (-> loc zip/down zip/right zip/right zip/right zip/node)
            operand4 (-> loc zip/down zip/right zip/right zip/right zip/right zip/node)
            clause (generate-quaternary-clause operator operand1 operand2 operand3 operand4 aggregation-method)]
        clause)
      :else (println "arietà sconosciuta"))))


(defn trim-par [s]
  (.substring  s 1 (- (count s) 1)))


(defn synth-formula-helper [loc aggregation-method]
  (if (= aggregation-method (AGGREGATION_METHOD :smart))
    (let [sentence (generate-text loc (AGGREGATION_METHOD :parenthesis))
          text (. realiser realiseSentence sentence)
          text (str/replace text #"\([^\(]*?\)" #(str pause (trim-par %1) pause))]
      text)
    (let [sentence (generate-text loc aggregation-method)
          text (. realiser realiseSentence sentence)]
      text)
    ))

(defn synth-formula [loc aggregation-method]
  ;(let [text (synth-formula-helper loc aggregation-method)]
  ;  (println text)
  ;  (synth-caller/say text synthesizer)
  ;  )
  (synth-formula-helper loc aggregation-method)
  )

(defn trim-breaks [sentence]
  (let [trimmed (string/replace sentence #"(^\s*<[^<]*>\s*)|(\s*<[^>]*>\s*(?=\.$))" "")]
    trimmed))

(defn replace-braces [sentence]
  (let [replaced-left (string/replace sentence #"\(" "<break time=\"100ms\"/> parentesi aperta <break time=\"100ms\"/>")
        replaced-also-right (string/replace replaced-left #"\)" "<break time=\"100ms\"/> parentesi chiusa <break time=\"100ms\"/>")]
    replaced-also-right))


(defn post-process-sentence [sentence]
  (let [trimmed-sentence (trim-breaks sentence)
        replaced-braces (replace-braces trimmed-sentence)
        cleaned-sentence (string/replace replaced-braces "'" "' ")
        initial-pause-sentence (str "Inizio formula: <break time=\"1000ms\"/>" cleaned-sentence)]
      initial-pause-sentence
    )
  )




(defn count-operator  [loc]
  (if (-> loc zip/end?)
    0
    (let [node (zip/node loc)
          inc (if (not-nil? (-> node :content))
                1
                0)]
      (+ inc (count-operator (zip/next loc))))
    ))


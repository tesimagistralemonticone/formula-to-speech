;This file is part of formula-to-speech.
;
;Formula-to-speech is free software: you can redistribute it and/or modify
;it under the terms of the GNU General Public License as published by
;the Free Software Foundation, either version 3 of the License, or
;(at your option) any later version.
;
;Formula-to-speech is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with formula-to-speech.  If not, see <https://www.gnu.org/licenses/>.

(ns application.latex-converter
  (:gen-class)
  (:require [clojure.string :as str]
            [clojure.java.io :as io])
  (:import (uk.ac.ed.ph.snuggletex SnuggleEngine XMLStringOutputOptions SnuggleInput)
           (uk.ac.ed.ph.snuggletex.upconversion.internal UpConversionPackageDefinitions)
           (uk.ac.ed.ph.snuggletex.upconversion UpConvertingPostProcessor)
           (org.apache.commons.lang3 SystemUtils))
  )

(def ^:const ITALIC_DIFF 119789)

(defn convert-with-snuggletex [input]
  (let [engine (doto (new SnuggleEngine) (. addPackage (UpConversionPackageDefinitions/getPackage)))
        session (doto(. engine createSession) (. parseInput(new SnuggleInput input)))
        up-converter (new UpConvertingPostProcessor)
        xml-string-output-options
          (doto
            (new XMLStringOutputOptions)
            (. addDOMPostProcessors (into-array uk.ac.ed.ph.snuggletex.DOMPostProcessor [up-converter]))
            (. setEncoding "UTF-8")
            (. setIndenting true))
        ]
    (. session buildXMLString xml-string-output-options))
  )


(defn invoke-latexml [formula-latex]
  (let [command-name (if (SystemUtils/IS_OS_WINDOWS) "latexml.bat" "latexml")
        builder (new ProcessBuilder (into-array [command-name "-"]))
        latexml (. builder start)
        out (. latexml getOutputStream)
        in (. latexml getInputStream)
        ]
    (doto out (. write (. formula-latex getBytes)) (. flush) (. close))
    (slurp in)))

(defn invoke-latexmlpost [input]
  (let [command-name (if (SystemUtils/IS_OS_WINDOWS) "latexml.bat" "latexml")
        builder (new ProcessBuilder (into-array ["latexmlpost" "-" "-cmml"]))
        latexml (. builder start)
        out (. latexml getOutputStream)
        in (. latexml getInputStream)
        ]
    (doto out (. write (. input getBytes)) (. flush) (. close))
    (slurp in)))

(defn extract-content [xml]
    (re-find #"(?s)<apply.+/apply>" xml))

(defn clean-tag-name [line]
  (let [cleaned-line (str/replace line #"(?<=</{0,1})m:(?=.+>)" "")]
    cleaned-line
    ))


(defn clean-italic [cmml]
  (letfn [(clean-char [italic-char] (-> (. italic-char codePointAt 0)(- ITALIC_DIFF) char str))]
    (str/replace cmml #"[\uD835\uDC4E-\uD835\uDC67]+" #(clean-char %1))))

(defn convert-with-latexml [formula-latex]
  (let [intermediate-out (invoke-latexml formula-latex)
        final-out (invoke-latexmlpost intermediate-out)
        cmml (-> final-out clean-tag-name extract-content)
        cmml-no-italic (clean-italic cmml)]
    cmml-no-italic)
  )

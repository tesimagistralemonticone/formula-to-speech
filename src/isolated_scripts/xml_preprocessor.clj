;This file is part of formula-to-speech.
;
;Formula-to-speech is free software: you can redistribute it and/or modify
;it under the terms of the GNU General Public License as published by
;the Free Software Foundation, either version 3 of the License, or
;(at your option) any later version.
;
;Formula-to-speech is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with formula-to-speech.  If not, see <https://www.gnu.org/licenses/>.


;\\TODO: da rimuovere
(ns isolated-scripts.xml-preprocessor
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))
;

(def current_path (System/getProperty "user.dir"))
(def output_dir (str current_path "/../../data_dev/output/"))
(def input_dir (str current_path "/../../data_dev/input/"))

(defn symbol->tag [line]
  (let [symbol-name (re-find #"(?<=<csymbol.*>)\w*(?=<)" line)]
    (if (nil? symbol-name)
      line
      (str/replace line #"(?<=<).*(?=>)" (str symbol-name "/")))))

(defn clean-line [line]
  (let [cleaned-line (str/replace line #"(?<=</{0,1})m:(?=.+>)" "")]
    cleaned-line
  ))

(defn save-res [file_name lines]
  (let [path (str output_dir file_name)]
    (with-open [w (-> path io/writer)]
      (doseq [line lines]
        (.write w line)
        (.newLine w)))))


(defn clean-xml-file [input_file_name output_file_name]
  (let [input_file_path (str input_dir input_file_name)]
    (with-open [rdr (-> input_file_path io/reader)]
      (let [lines (line-seq rdr)
            cleaned-lines (map clean-line lines)
            replaced-lines (map symbol->tag cleaned-lines)]
        (save-res output_file_name replaced-lines)))))

(clean-xml-file "h4.xml" "h4.xml")

(ns util.config-manager
  (:require [clojure.java.io :as io]
            [toml.core :as toml]))

(def ^:const configuration (toml/read (-> "config.toml" io/resource slurp ) :keywordize))

;(println (-> configuration :pause-length))
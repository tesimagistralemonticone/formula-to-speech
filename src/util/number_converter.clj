;This file is part of formula-to-speech.
;
;Formula-to-speech is free software: you can redistribute it and/or modify
;it under the terms of the GNU General Public License as published by
;the Free Software Foundation, either version 3 of the License, or
;(at your option) any later version.
;
;Formula-to-speech is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with formula-to-speech.  If not, see <https://www.gnu.org/licenses/>.


(ns util.number-converter
  (:require [clojure.math.numeric-tower :as math]
            [clojure.string :as string]))

(def vocals-restricted #{\a \e \i \u})

(def base-ordinal-numbers
  {0   "zero"
   1   "uno"
   2   "due"
   3   "tre"
   4   "quattro"
   5   "cinque"
   6   "sei"
   7   "sette"
   8   "otto"
   9   "nove"
   10  "dieci"
   11  "undici"
   12  "dodici"
   13  "tredici"
   14  "quattordici"
   15  "quindici"
   16  "sedici"
   17  "diciasette"
   18  "diciotto"
   19  "diciannove"
   20  "venti"
   30  "trenta"
   40  "quaranta"
   50  "cinquanta"
   60  "sessanta"
   70  "settanta"
   80  "ottanta"
   90  "novanta"
   100 "cento"
   200 "duecento"
   300 "trecento"
   400 "quattrocento"
   500 "cinquecentocento"
   600 "seicento"
   700 "settecento"
   800 "ottocentocento"
   900 "novecento"})

(def base-cardinal-numbers
  {1  "prima"
   2  "seconda"
   3  "terza"
   4  "quarta"
   5  "quinta"
   6  "sesta"
   7  "settima"
   8  "ottava"
   9  "nona"
   10 "decima"})

(def huge-numbers
  {0 {:single "mille" :multiple "mila"}
   1 {:single "un milione" :multiple "milioni"}
   2 {:single "un miliardo" :multiple "miliardi"}
   })

; Calcola il numero di cifre del numero sfruttando il logaritmo in base 10.
; Nel caso il numero sia zero (logaritmo non definito) si restituisce il valore 1
(defn length-of [n]
  (if (= n 0)
    1
    (-> (Math/log10 n) int (+ 1))))


(defn string->number [str]
  (let [n (read-string str)]
    (if (number? n) n nil)))

; Combina due numeri (parole) eliminando l'ultimo carattere del primo numero nel caso
; in caso il primo numero termini con vocale e il secondo inizi per vocale.
; Es ventI Uno -> ventuno
; Nota, si considera un insieme delle vocali ristretto (manca la o) in quanto si preferisce
; evitare l'elisione con i numeri composti da cento
; Es centouno è più usato di centuno anche se entrambi sono corretti
(defn smart-combine [head tail]
  (let [h (last head)
        t (first tail)]
    (if (and (contains? vocals-restricted h)
             (contains? vocals-restricted t))
      (str (subs head 0 (- (count head) 1)) tail)
      (str head tail))))

; Converte un numero (cifra), compreso tra 0 e 999 estremi inclusi, nella corrispondente parola
(defn small-number-to-word [n]
  (cond
    (= n 0) ""
    (< n 21) (-> base-ordinal-numbers (get n))
    :else
    (let [length (length-of n)
          n-clean (math/expt 10 (- length 1))               ;es 425 -> 100 o 1250 -> 1000
          head-number (* (quot n n-clean) n-clean)
          tail-number (mod n n-clean)
          head-word (-> base-ordinal-numbers (get head-number))
          tail-word (small-number-to-word tail-number)]
      (smart-combine head-word tail-word))))


(defn huge-number-to-word-helper [n level]
  (if (= n 0)
    ""
    (let [rest (huge-number-to-word-helper (quot n 1000) (+ level 1))
          word-number
          (cond
            (= (mod n 1000) 0) ""
            (= (mod n 1000) 1) (-> huge-numbers (get level) (get :single))
            :else
              (let [fst (small-number-to-word (mod n 1000))
                    snd (-> huge-numbers (get level) (get :multiple))]
                (str fst " " snd)))]                             ; 100000 -> fst = 100, snd = mila
      (str rest " " word-number))))

; Converte un numero (cifra), compreso tra 1000 e 999 miliardi estremi inclusi, nella corrispondente parola
(defn huge-number-to-word [n]
  (huge-number-to-word-helper n 0))

; converte un genrico numero (cifra) nella corrispondente parola rappresentante il numero ordinale
(defn cardinal->ordinal [n]
  (let [small-number (mod n 1000)
        huge-number (quot n 1000)
        small-word (small-number-to-word small-number)
        huge-word (huge-number-to-word huge-number)]
    (string/trim (str huge-word " " small-word))))

; converte un genrico numero (cifra) nella corrispondente parola rappresentante il numero cardinale femminile
(defn ordinal->cardinal [n]
  (if (< n 11)
    (-> base-cardinal-numbers (get n))
    (let [ordinal-word (cardinal->ordinal n)]
      (smart-combine ordinal-word "esima"))))


;This file is part of formula-to-speech.
;
;Formula-to-speech is free software: you can redistribute it and/or modify
;it under the terms of the GNU General Public License as published by
;the Free Software Foundation, either version 3 of the License, or
;(at your option) any later version.
;
;Formula-to-speech is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with formula-to-speech.  If not, see <https://www.gnu.org/licenses/>.

(ns util.dictionary-manager
  (:require [clojure.java.io :as io]
            [clojure.data.json :as json]))

(def ^:const LANGUAGE {:ita 1 :eng 2})

(def ^:const DICTIONARY_PATH {
                              :operators-it "dictionaries/operators-it.json"})


(defn load-operator-dic [path]
  (-> path io/resource io/reader (json/read :key-fn keyword)))


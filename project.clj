(defproject formula-to-text "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :repositories [["www2.ph.ed.ac.uk-releases" "https://www2.ph.ed.ac.uk/maven2/"]
                 ["local_repo" {:url "file:local_repo"
                                :username "a"
                                :password "a"}]]
  :resource-paths ["resources" "lib"]
  ;:resource-paths ["resources/lexicon" ]
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.zip "0.1.3"]
                 [org.clojure/math.numeric-tower "0.0.4"]
                 [uk.ac.ed.ph.snuggletex/snuggletex-core "1.2.2"]
                 [uk.ac.ed.ph.snuggletex/snuggletex-upconversion "1.2.2"]
                 [uk.ac.ed.ph.snuggletex/snuggletex-jeuclid "1.2.2"]
                 [org.clojure/core.match "0.3.0-alpha5"]
                 [org.clojure/data.json "0.2.6"]
                 [com.ibm.watson.developer_cloud/java-sdk "6.13.1"]
                 [at.unisalzburg.dbresearch/apted "1"]
                 [com.mazzei.simplenlg-it/simplenlg-it "1"]
                 [org.clojure/algo.generic "0.1.0"]
                 ;; https://mvnrepository.com/artifact/org.apache.commons/commons-lang3
                 [org.apache.commons/commons-lang3 "3.0"]
                 [toml "0.1.3"]
                 [org.clojure/tools.cli "0.4.2"]
                 ]
  ;:main application.latex-converter
  ;:aot [application.latex-converter]
  :main application.core
  :aot [application.core]
  )
